var btnAdicionar = document.getElementById("idBtnAdicionar");
var btnRelatorio = document.getElementById("idBtnRelatorio");
var tabela = document.getElementById("idTabela");
var arrayPizzas = [];

btnAdicionar.addEventListener("click", function(event){
    event.preventDefault();
    let verificar = verificarTamanhos();

    if(verificar){
        let formulario = document.getElementById("idFormulario");
        let pizzas = obterPizzas(formulario);

        let area = calculoArea(pizzas);
        pizzas.areaPizza = area;

        let custo = calculoCusto(pizzas, area);
        pizzas.custoCmPizza = custo;
        
        arrayPizzas.push(pizzas);

        alert("Pizza Cadastrada com Sucesso!")
        limparCampos();
    }

});

btnRelatorio.addEventListener("click", function(event){
    event.preventDefault();
    tabela.style = "display: block";
    let formulario = document.getElementById("idFormulario");
    formulario.style = "display: none";
    let pizzas = obterPizzas(formulario);
    let arrayPizzas = ordenarPizzas(pizzas);
    montarTabela(arrayPizzas);
});

function limparCampos(){
    document.getElementById("idNomePizza").value = "";
    document.getElementById("idTamanhoPizza").value = "";
    document.getElementById("idPrecoPizza").value = "";
}

function verificarTamanhos(){
    let tamanhoDaPizza = document.getElementById("idTamanhoPizza").value;
    let verificar = true;
    for (let pizzas of arrayPizzas){
        if(tamanhoDaPizza == pizzas.tamanhoPizza){
            alert("Tamanho já cadastrado, tente novamente")
            verificar = false;
        }
    }

    return verificar
}

function obterPizzas(formulario){
    let pizzas = {
        nomePizza : formulario.nmNomePizza.value, 
        tamanhoPizza : formulario.nmTamanhoPizza.value, 
        precoPizza : formulario.nmPrecoPizza.value,
        areaPizza: "",
        custoCmPizza: "",
        diferencaCb: ""
    } 

    return pizzas
}

function calculoArea(pizzas){
    let area = ((3.14)*((pizzas.tamanhoPizza)/2)**2);

    return area 
}

function calculoCusto(pizzas, area){
    let custo = ((pizzas.precoPizza)/area).toFixed(2);
    
    return custo
}

function ordenarPizzas(pizzas){
    arrayPizzas.sort(function (a, b) {
        return a.custoCmPizza - b.custoCmPizza
    })

    return arrayPizzas
}

function calculoCb(arrayPizzas){
    arrayPizzas[0].diferencaCb = "Melhor CB"
    for(let i = 1; i < arrayPizzas.length; i++){
        arrayPizzas[i].diferencaCb = (((arrayPizzas[i].custoCmPizza / arrayPizzas[i-1].custoCmPizza)-1)*100).toFixed(2);
    }
}

function montarTabela(arrayPizzas){
    calculoCb(arrayPizzas);
    
    for(let pizzas of arrayPizzas){
        let tBody = document.getElementById("idTbody");
        let pizzasTr = document.createElement("tr");
        tBody.appendChild(pizzasTr);
        pizzasTr.appendChild(montaTd(pizzas.nomePizza))
        pizzasTr.appendChild(montaTd(pizzas.tamanhoPizza + " cm"))
        pizzasTr.appendChild(montaTd("R$ " + pizzas.precoPizza))
        pizzasTr.appendChild(montaTd("R$ " + pizzas.custoCmPizza))
        pizzasTr.appendChild(montaTd(pizzas.diferencaCb + " %"))
    }
}
    
function montaTd(dado) {
    let pizzaTd = document.createElement("td");
    pizzaTd.textContent = dado;
    
    return pizzaTd
}
